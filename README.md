# Input System Wrapper

This is a snippet of a handy wrapper for new Unity Input system. The main reason of its creation was gaining more control over the execution of input events, as in older verisons of the system there was a prominent issue with execution of events the callbacks of which have been added at the current frame.
The project provides a demonstration of the issue and its solution by the wrapper. Pressing `Enter` should cause one object to deactivate and the other to activate - in the old version of input system however, the enabled object subscribes to callback with `OnEnable` and then executes it's own callback reactivation the original object.

The issue seems to be solved in the newer version of Input System, nevertheless the wrapper is a handy tool for debugging, throwing warnigns when two identical callbacks get added (usually a result of a bug, or a cause of one - rarely ever a desired behaviour) and a basis for any other project-sepcyfic extensions.

The other issue that this wrapper tackles, is the fact that UI event actions seemed not to work properly with modifiers such as `ReleaseOnly` - due to somewhat manual input action reading in the basic `InputSystemUIInputModule`. To overcome that, `UIInputInterface` provides possibility to execute basic UI navigation events with custom events utilizing all their configuration in the InputAction asset. Currently supports basic navigation and progression - pointer still handled natively.

Additionally `InputManager` class comes with manual device and mouse auto-swapping mechanisms, useful in case of certain target platforms, and a robust logging capability that comes in handy when issues with multiplayer input arise.
