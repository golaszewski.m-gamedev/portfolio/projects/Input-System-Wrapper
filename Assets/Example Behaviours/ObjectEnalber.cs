﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using InputSystemWrapperExample.Input;

namespace InputSystemWrapperExample
{
    public class ObjectEnalber : MonoBehaviour
    {
        [SerializeField]
        private GameObject objectToEnable;

        [SerializeField]
        private bool useWrappedSystem;

        private void OnEnable()
        {
            if (useWrappedSystem)
            {
                InputManager.Instance.InputActions.Submit.performed += EnableOtherObjectCallback;
            }
            else
            {
                InputManager.Instance.PlayerInput.actions.FindActionMap("Menu").FindAction("Submit").performed += EnableOtherObjectCallback;
            }
        }

        private void OnDisable()
        {
            if (useWrappedSystem)
            {
                InputManager.Instance.InputActions.Submit.performed -= EnableOtherObjectCallback;
            }
            else
            {
                InputManager.Instance.PlayerInput.actions.FindActionMap("Menu").FindAction("Submit").performed -= EnableOtherObjectCallback;
            }
        }

        private void EnableOtherObjectCallback(InputAction.CallbackContext context) 
        {
            objectToEnable.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}